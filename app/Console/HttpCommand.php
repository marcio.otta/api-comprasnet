<?php

namespace App\Console;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Exception\GuzzleException;
use Spatie\GuzzleRateLimiterMiddleware\RateLimiterMiddleware;

// use GuzzleHttp\Exception\RequestException;
// use GuzzleHttp\Exception\ClientException;

class HttpCommand extends Command
{
    /**
     * The Guzzle object
     *
     * @var guzzle
     */
    protected $client;

    /**
     * The API url
     *
     * @var string
     */
    protected $base_url;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $stack = HandlerStack::create();
        $stack->push(RateLimiterMiddleware::perMinute(50));

        // $this->client = new Client();
        $this->client = new Client([
            'verify' => false,
            'handler' => $stack,
        ]);
    }

    /**
     * Busca dados
     *
     * @return mixed
     */
    public function getData($extended_url, $full_url = false)
    {
        $this->base_url = $full_url ? $extended_url : config('comprasnet.base_url') . $extended_url;

        $this->comment(date('[Y-m-d H:i:s]'));

        try {
            $result = $this->client->get($this->base_url);

            if ($result && $result->getStatusCode() == 200) {
                $this->info('Requisição realizada com Sucesso!');
                $this->line('Código de resposta: ' . $result->getStatusCode());
                $this->line('Header: ' . $result->getHeader('content-type')[0]);

                $response = json_decode($result->getBody(), true);
                return $response;
            } else {
                $this->error('Houve algum erro na request...');
                $this->error('Código de resposta: ' . $result->getStatusCode());
                $this->line('----------------------------------------------------------------------');
                return false;
            }
        } catch (GuzzleException $e) {
            $this->error('Houve algum erro na request...');

            $this->error('getRequest:');
            $this->error(Psr7\str($e->getRequest()));

            $this->error('getResponse:');
            if ($e->hasResponse()) {
                $this->error(Psr7\str($e->getResponse()));
            }

            $this->error('fullError:');
            $this->error($e);

            $this->line('----------------------------------------------------------------------');
        }
    }
}
