#
# Script base para Aplicações Laravel
#

#
# Container temporário para instalar as dependências via Composer
# 
FROM composer:1.9.2 as composer
WORKDIR /app
COPY composer.json composer.json
RUN composer --no-ansi install --no-ansi --prefer-dist --no-dev --no-scripts --no-interaction --ignore-platform-reqs --no-autoloader

# 
# Container da Aplicação
#
# FROM php:7.4.2-fpm
# FROM php:7.3.10-fpm
FROM php:7.2-fpm

# Configura Timezone
ENV TZ=America/Sao_Paulo

# Requerido para baixar dados da Microsoft
ENV ACCEPT_EULA=Y

# Instala pacotes requeridos do sistema
RUN apt-get update \
    && apt-get -y --no-install-recommends install \
    cron \
    locales \
    apt-transport-https \
    tzdata \
    curl \
    procps \
    vim \
    gnupg \
    libxml2-dev \
    libonig-dev

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen 
RUN locale-gen

# Adiciona chaves da Microsoft e source list no apt
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/9/prod.list > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get update

# Instalar ODBC Driver e SQL Command Line Utility para o SQL Server
RUN apt-get install -y msodbcsql17 mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile \
    && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
RUN apt-get install -y unixodbc-dev

# RUN ACCEPT_EULA=Y apt-get install -y msodbcsql17 unixodbc-dev cron curl openjdk-11-jdk ant ca-certificates-java npm && apt-get clean
# RUN update-ca-certificates -f
# RUN wget http://security-cdn.debian.org/debian-security/pool/updates/main/o/openssl1.0/libssl1.0.2_1.0.2u-1~deb9u1_amd64.deb     && dpkg -i libssl1.0.2_1.0.2u-1~deb9u1_amd64.deb
# RUN echo "[ODBC Driver 17 for SQL Server]\n          Driver = ODBC Driver 17 for SQL Server" >> /etc/odbc.ini

# Instalar composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Adiciona PDO Driver no PHP
# RUN pecl install sqlsrv pdo_sqlsrv
RUN pecl install pdo_sqlsrv
RUN echo "extension=pdo_sqlsrv.so" > /usr/local/etc/php/conf.d/docker-php-ext-pdo_sqlsrv.ini
# RUN echo "extension=sqlsrv.so" > /usr/local/etc/php/conf.d/docker-php-ext-sqlsrv.ini

# Corrige Cipher para o sqlsrv ssl
RUN sed -i "/CipherString/c\CipherString = DEFAULT@SECLEVEL=1" /etc/ssl/openssl.cnf

# RUN docker-php-ext-enable --ini-name 30-sqlsrv.ini sqlsrv
# RUN docker-php-ext-enable --ini-name 35-pdo_sqlsrv.ini pdo_sqlsrv

# RUN docker-php-source delete

# Adiciona dependências do Laravel no PHP
RUN docker-php-ext-install \
    bcmath  \
    ctype  \
    json \
    mbstring \
    pdo \
    tokenizer \
    xml

# Installing new Simple SMTP (msmtp)
RUN apt-get update && \
    apt-get install -y --no-install-recommends msmtp && \
    # echo "FromLineOverride=YES" >> /etc/msmtprc && \
    # chmod 600 /etc/msmtprc && \
    echo 'sendmail_path = "/usr/bin/msmtp -t"' > /usr/local/etc/php/conf.d/mail.ini

# Limpa pacotes de instalação do apt
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Define Workdir
WORKDIR /app

# Copy application from composer temporary container
COPY --from=composer /app /app

# Copy source to container
COPY . /app

# Create composer autoloader
RUN composer --no-ansi dump-autoload --no-scripts --no-dev --no-interaction --optimize

# Optimize application (artisan)
# RUN php artisan clear-compiled \
#     && php artisan cache:clear \
#     && php artisan config:clear \
#     && php artisan config:cache \
#     && php artisan optimize

# Config application storage folder
# RUN chown -R www-data:www-data ./app/storage/

# Configura alias
RUN echo "alias l='ls -la'" >> ~/.bash_profile \
    && echo "alias l='ls -la'" >> ~/.bashrc
RUN echo "alias a='php artisan'" >> ~/.bash_profile \
    && echo "alias a='php artisan'" >> ~/.bashrc


# Abrir porta
EXPOSE 9000

# Configura os scripts do doker na variável $PATH
ENV PATH $PATH:/app/docker/entrypoint

# Copia status.sh para a pasta de monitoramento
COPY ./docker/entrypoint/status.sh /monitoramento/

# Convert os arquivos executáveis
RUN chmod +x ./docker/entrypoint/*
RUN chmod +x /monitoramento/*

# Executya o script inicial
ENTRYPOINT ["entrypoint.sh"]

# Inicia o php-fpm
CMD ["php-fpm"]
